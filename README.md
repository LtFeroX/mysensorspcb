﻿# MySensors AiO PCB NODE

A PCB of everything you need for creating a sensor NODE for MySensors

## Including :
* µC with Arduino Bootloader  
* FTDI Connector  
* NRF24L01+ (SMD)  
* Sensor Sockets  
* PCB-Layout  

### Prerequisites
Hardware needed to build Project:
* PCB + Hardware  
* USB-FTDI  
* Battery Holder (2x AA - 3VDC)  
* MySensors Sketch  

### PARTLIST:

| Part | Value   | Device                              | Package       | Library           | Sheet |  
| ---- | ------- | ----------------------------------- | ------------- | ----------------- | ----- | 
| C1   | 0.1uF   | C-EU025-025X050                     | C025-025X050  | rcl               | 1     |  
| C2   | 0.1uF   | C-EU025-025X050                     | C025-025X050  | rcl               | 1     |  
| C3   | 0.1uF   | C-EU025-025X050                     | C025-025X050  | rcl               | 1     |  
| C4   | 0.1uF   | C-EU025-025X050                     | C025-025X050  | rcl               | 1     |  
| C5   | 0.1uF   | C-EU025-025X050                     | C025-025X050  | rcl               | 1     |  
| C6   | 10uF    | CAP_POL1206                         | EIA3216       | SparkFun          | 1     |  
| C7   | 68uF    | CAP_POL3528                         | EIA3528       | SparkFun          | 1     |  
| D1   |         | DIODESMA                            | SMA-DIODE     | SparkFun          | 1     |  
| IC1  |         | MEGA48/88/168-AU                    | TQFP32-08     | avr-6             | 1     |  
| JP1  |         | 2X09/90                             |               | pinhead           | 1     |  
| JP2  |         | PINHD-2X3                           |  2X03         | pinhead           | 1     |  
| JP3  |         | 1X03                                |               | SparkFun          | 1     |  
| L1   | 47uH    | INDUCTORCR54                        | CR54          | SparkFun          | 1     |  
| Q1   | 8 MHz   | CSM-7X-DU                           |               | crystal           | 1     |   
| R1   | 10k     | R-EU_M0805                          | M0805         | rcl               | 1     |  
| S1   | RESET   | MOMENTARY-SWITCH-SPST-SMD-6.0X3.5MM | SMD_6.0X3.5MM | SparkFun-Switches | 1     |  
| U$1  |         | NRF24L01_MINI                       | NRF24L01_SMD  | 11_OWN_library    | 1     |  
| U1   | NCP1402 | V_REG_NCP1400SOT23-5                | SOT23-5       | SparkFun          | 1     |
----------------------------------------------------------------------------------------------------  

### Getting started
to be continued...

### Table of content  

* EAGLE --> Eagle files for designing schematic and board
* Docs --> PDF exported by EAGLE  

----------------------------

